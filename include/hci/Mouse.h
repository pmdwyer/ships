#ifndef _MOUSE_H
#define _MOUSE_H

#include <bitset>

#include <SFML/System/Vector2.hpp>

#include "hci/KeyState.h"

namespace pmd::hci
{
  static constexpr int NumButtons = 5;
  
  using MouseKeys = std::bitset<NumButtons>;

  struct mouse
  {
    MouseKeys buttons;
    sf::Vector2i pos = {0, 0};
   };

   using Mouse = struct mouse;
}

#endif // _MOUSE_H