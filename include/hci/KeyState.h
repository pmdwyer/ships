#ifndef _KEYSTATE_H
#define _KEYSTATE_H

namespace pmd::hci
{
  enum class KeyState
  {
    Released,
    Pressed
   };
}

#endif // _KEYSTATE_H