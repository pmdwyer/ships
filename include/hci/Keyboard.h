#ifndef _KEYBOARD_H
#define _KEYBOARD_H

#include <bitset>
#include <vector>

#include "hci/KeyState.h"
#include "hci/Key.h"

namespace pmd::hci
{
  static constexpr int NumKeys = 128;

  typedef std::bitset<NumKeys> Keys;

  class Keyboard
  {
    public:
      Keyboard() = default;
      ~Keyboard() = default;

      static Keys make_chord(const std::vector<Key>& keys);

      Keys keys;

      bool contains(const Keys& keys) const;
  };
}

#endif // _KEYBOARD_H