#ifndef _TILE_H
#define _TILE_H

namespace pmd::game
{
  class Tile
  {
    public:
      Tile() = default;
      ~Tile() = default;

      static constexpr int Width = 32;
      static constexpr int Height = 32;
      
    private:
  };
}

#endif // _TILE_H