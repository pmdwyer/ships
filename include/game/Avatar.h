#ifndef _AVATAR_H
#define _AVATAR_H

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>

namespace pmd::game
{
  class Avatar
  {
    public:
      Avatar() = default;
      ~Avatar() = default;

      void draw(sf::RenderTarget& target);
      
      sf::Vector2i pos;
      sf::Vector2f screenPos;
      sf::CircleShape shape;
  };
}

#endif // _AVATAR_H