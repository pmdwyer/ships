#ifndef _MAP_H
#define _MAP_H

#include <vector>

#include <SFML/Graphics.hpp>

#include "game/Tile.h"

namespace pmd::game
{
  class Map
  {
    public:
      Map(sf::Vector2i size);
      ~Map() = default;

      void draw(sf::RenderTarget& target);

    private:
      sf::Vector2i m_size;
      std::vector<Tile> m_tiles;
      std::vector<sf::CircleShape> m_centers;
  };
}

#endif // _MAP_H