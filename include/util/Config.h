#ifndef _CONFIG_H
#define _CONFIG_H

#include <string>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace pmd::util
{
  class Config
  {
    public:
      Config(const Config&) = delete;
      Config(Config&&) = delete;
      Config& operator= (const Config&) = delete;
      Config& operator= (Config&&) = delete;

      static Config& instance();
      bool init(const std::string& cfg_path);

      json config;

    private:
      Config() = default;
      ~Config() = default;
  };
}

#endif // _CONFIG_H