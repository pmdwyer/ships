#ifndef _MATH_H
#define _MATH_H

namespace pmd::util
{
  class Math
  {
    public:
      Math() = default;
      ~Math() = default;

      template <typename T>
      static T lerp(T start, T end, float amount)
      {
        return start + end * amount;
      }
  };
}

#endif // _MATH_H