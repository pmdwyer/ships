#ifndef _THREADUTILS_H
#define _THREADUTILS_H

#include <string>
#include <thread>

using namespace std;

namespace pmd::util
{
  std::string thread_id_to_str(const std::thread::id& id);
}

#endif // _THREADUTILS_H