#ifndef _FILEUTILS_H
#define _FILEUTILS_H

#include <string>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

namespace pmd::util
{
  bool file_exists(const std::string& path);
}

#endif // _FILEUTILS_H