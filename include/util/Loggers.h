#ifndef _LOGGERS_H
#define _LOGGERS_H

#include <string>
#include <memory>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace pmd::util
{
  class Loggers
  {
    public:
      Loggers() = default;
      ~Loggers() = default;

      bool init();

      static const std::string Console;

    private:
      std::shared_ptr<spdlog::logger> m_logger;
   };
}

#endif // _LOGGERS_H