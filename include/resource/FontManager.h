#ifndef _FONTMANAGER_H
#define _FONTMANAGER_H

#include <string>
#include <unordered_map>

#include <SFML/Graphics.hpp>

namespace pmd::resource
{
  class FontManager
  {
    public:
      ~FontManager() = default;

      FontManager(const FontManager&) = delete;
      FontManager(FontManager&&) = delete;
      FontManager& operator=(const FontManager&) = delete;
      FontManager& operator=(FontManager&&) = delete;

      static FontManager& instance();

      bool init();
      sf::Font& get_font(const std::string& name);
      
    private:
      std::string m_fontpath;
      std::unordered_map<std::string, sf::Font> m_fonts;

      FontManager() = default;

  };
}

#endif // _FONTMANAGER_H