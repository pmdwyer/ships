#ifndef _GAMESTATE_H
#define _GAMESTATE_H

#include <SFML/Graphics.hpp>

#include "engine/IState.h"
#include "game/Map.h"
#include "game/Avatar.h"

using namespace pmd::game;

namespace pmd::engine
{
  class GameState : public IState
  {
    public:
      GameState();
      ~GameState() = default;

      bool init(StateManager* manager) override;
      void start() override;
      void pause() override;
      void stop() override;
      void destroy() override;
      void update(float dt) override;
      void draw(sf::RenderTarget& target) override;

    private:
      sf::CircleShape m_circle;
      Map m_map;
      Avatar m_player;

      void pan_camera(float dx, float dy);
  };
}

#endif // _GAMESTATE_H