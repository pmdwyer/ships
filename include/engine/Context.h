#ifndef _CONTEXT_H
#define _CONTEXT_H

#include <SFML/Graphics.hpp>

#include "engine/EventManager.h"

namespace pmd::engine
{
  class Context
  {
    public:
      Context() = default;
      ~Context() = default;
      EventManager event_manager;
      sf::RenderWindow window;
  };
}

#endif // _CONTEXT_H