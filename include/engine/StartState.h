#ifndef _STARTSTATE_H
#define _STARTSTATE_H

#include "engine/IState.h"
#include "ui/Button.h"

using namespace pmd::ui;

namespace pmd::engine
{
  class StartState : IState
  {
    public:
      StartState() = default;
      ~StartState() = default;

      bool init(StateManager* manager) override;
      void start() override;
      void pause() override;
      void stop() override;
      void destroy() override;

      void update(float dt) override;
      void draw(sf::RenderTarget& target) override;

    private:
      std::weak_ptr<Context> m_context;
      StateManager *m_manager = nullptr;
      unsigned int m_selection = 0;
  };
}

#endif // _STARTSTATE_H