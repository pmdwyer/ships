#ifndef _ACTION_H
#define _ACTION_H

namespace pmd::engine
{
  enum class Action
  {
    FocusLost,
    FocusGained,
    WindowResize,
    PanLeft,
    PanRight,
    PanUp,
    PanDown,
    MoveLeft,
    MoveRight,
    MoveUp,
    MoveDown
  };
}

#endif // _ACTION_H