#ifndef _GAMECLIENT_H
#define _GAMECLIENT_H

#include <memory>

#include "engine/Context.h"
#include "engine/StateManager.h"

namespace pmd::engine
{
  class GameClient
  {
    public:
      GameClient() = default;
      ~GameClient() = default;

      bool init();
      void run();

    private:
      StateManager m_state_manager;
  };
}

#endif // _GAMECLIENT_H