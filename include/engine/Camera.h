#ifndef _CAMERA_H
#define _CAMERA_H

#include <SFML/Graphics.hpp>

namespace pmd::engine
{
  class Camera
  {
    public:
      Camera() = default;
      ~Camera() = default;

      sf::View view;
  };
}

#endif // _CAMERA_H