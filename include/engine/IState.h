#ifndef _ISTATE_H
#define _ISTATE_H

#include <SFML/Graphics.hpp>

#include "engine/StateType.h"
#include "engine/Context.h"
#include "engine/StateManager.h"
#include "engine/Camera.h"

namespace pmd::engine
{
  class IState
  {
    public:
      IState() = default;
      virtual ~IState() {};

      virtual bool init(StateManager* manager) = 0;
      virtual void start() = 0;
      virtual void pause() = 0;
      virtual void stop() = 0;
      virtual void destroy() = 0;
      virtual void update(float dt) = 0;
      virtual void draw(sf::RenderTarget& target) = 0;

      StateType type;
      StateManager *m_manager = nullptr;
      Camera camera;
  };
}

#endif // _ISTATE_H