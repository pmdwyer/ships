#ifndef _STATEMANAGER_H
#define _STATEMANAGER_H

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <SFML/Graphics.hpp>

#include "engine/StateType.h"
#include "engine/Context.h"

using namespace std;

namespace pmd::engine
{
  class IState;

  class StateManager
  {
    public:
      StateManager() = default;
      ~StateManager();

      bool init();

      void switch_to(StateType type);
      void push(StateType type);

      void update(float dt);
      void draw(sf::RenderTarget& target);
      void cleanup();

      Context* context = nullptr;

    private:
      std::unordered_map<StateType, std::function<IState*()>> m_state_factory;
      std::unordered_map<StateType, IState*> m_state_cache;
      std::vector<IState*> m_states;
      StateType m_state_to_swap = StateType::Unknown;

      IState* find_or_create_state(StateType type);
  };
}

#endif // _STATEMANAGER_H