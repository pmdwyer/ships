#ifndef _EVENTMANAGER_H
#define _EVENTMANAGER_H

#include <functional>
#include <unordered_map>

#include <SFML/Window/Event.hpp>

#include "engine/Action.h"
#include "hci/Keyboard.h"
#include "hci/Mouse.h"

using namespace pmd::hci;

namespace pmd::engine
{
  class EventManager
  {
    public:
      EventManager() = default;
      ~EventManager() = default;

      bool init();

      void update();

      void record_event(const sf::Event& e);
      void register_for_action(Action a, std::function<void()> cb);

      Keyboard k;
      Mouse m;

    private:
      enum WindowEdges
      {
        Center,
        Top,
        Bottom,
        Left,
        Right
      };

      WindowEdges m_mouse_loc = WindowEdges::Center;

      std::unordered_map<Action, std::function<void()>> m_action_events;
      std::unordered_map<Keys, Action> m_key_map;
      std::unordered_map<MouseKeys, Action> m_mouse_key_map;

      void check_and_update_bounds(sf::Vector2i pos);
      void check_and_fire_action(Action a);
  };
}

#endif // _EVENTMANAGER_H