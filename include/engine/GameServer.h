#ifndef _GAMESERVER_H
#define _GAMESERVER_H

#include <thread>

namespace pmd::engine
{
  class GameServer
  {
    public:
      GameServer() = default;
      ~GameServer();

      GameServer(GameServer&&) = delete;
      GameServer(const GameServer&) = delete;
      GameServer& operator=(GameServer&&) = delete;
      GameServer& operator=(const GameServer&) = delete;

      bool init();
      void run();
      void stop();

    private:
      bool m_islocal = true;
      bool m_isrunning = false;
      std::thread m_localThread;
   };
}

#endif // _GAMESERVER_H
