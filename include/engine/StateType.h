#ifndef _STATETYPE_H
#define _STATETYPE_H

namespace pmd::engine
{
  enum StateType
  {
    Unknown,
    Intro,
    Menu,
    Game,
    Outro
  };
}

#endif // _STATETYPE_H