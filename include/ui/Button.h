#ifndef _BUTTON_H
#define _BUTTON_H

#include <string>
#include <functional>

#include <SFML/Graphics.hpp>

namespace pmd::ui
{
  class Button
  {
    public:
      Button() = default;
      ~Button() = default;

      void set_text(const std::string& t, sf::Vector2f p, unsigned int sz);

      void on_click(std::function<void(sf::Vector2f)> cb);
      void click(sf::Vector2f point);

      void draw(sf::RenderTarget& target);

    private:
      sf::Text m_drawable_text;
      std::function<void(sf::Vector2f)> m_callback;
  };
}

#endif // _BUTTON_H