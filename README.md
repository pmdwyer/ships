## TODO
* add window size to eventmanager
* panning camera
* config singleton class
* create a menu
  * relative screen space
  * buttons
    * callbacks
* file log
* handle window loses focus
  * clear keys
* event manager event registration
* window crashes on scroll lock / printscreen
* lerp moves
* add tiles