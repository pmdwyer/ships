#include "resource/FontManager.h"

#include "util/Config.h"

using namespace pmd::resource;
using namespace pmd::util;

FontManager& FontManager::instance()
{
  static FontManager instance;
  return instance;
}

bool FontManager::init()
{
  m_fontpath = Config::instance().config["assets"]["assetpath"];
  m_fontpath += '/';
  m_fontpath += Config::instance().config["assets"]["fonts"]["fontpath"];
  return true;
}

sf::Font& FontManager::get_font(const std::string& name)
{
  if (m_fonts.find(name) == m_fonts.end())
  {
    std::string path = m_fontpath + '/';
    path += Config::instance().config["assets"]["fonts"][name];
    m_fonts[name] = sf::Font();
    m_fonts[name].loadFromFile(path);
  }
  return m_fonts[name];
}