#include "ui/Button.h"

#include "resource/FontManager.h"

using namespace pmd::ui;
using namespace pmd::resource;

void Button::set_text(const std::string& t, sf::Vector2f p, unsigned int sz)
{
  FontManager::instance().init();
  m_drawable_text.setFont(FontManager::instance().get_font("hack-reg"));
  m_drawable_text.setString(t);
  m_drawable_text.setPosition(p);
  m_drawable_text.setCharacterSize(sz);
}

void Button::draw(sf::RenderTarget& target)
{
  target.draw(m_drawable_text);
}

void Button::on_click(std::function<void(sf::Vector2f point)> cb)
{
  m_callback = cb;
}

void Button::click(sf::Vector2f point)
{
  m_callback(point); 
}