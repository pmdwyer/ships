#include <nlohmann/json.hpp>

#include "engine/GameClient.h"
#include "engine/GameServer.h"
#include "util/Loggers.h"
#include "util/Config.h"

using namespace pmd::engine;
using namespace pmd::util;

using json = nlohmann::json;

int main(int argc, char* argv[])
{
  Loggers loggers;
  loggers.init();

  if (argc < 2)
  {
    return -1;
  }

  spdlog::get(Loggers::Console)->debug("calling config with {}", argv[1]);
  if (!Config::instance().init(argv[1]))
  {
    return 1;
  }

  GameServer server;
  spdlog::get(Loggers::Console)->debug("starting game server");
  if (!server.init())
  {
    return 2;
  }

  GameClient client;
  spdlog::get(Loggers::Console)->debug("starting game client");
  if (client.init())
  {
    client.run();
  }

  server.stop();
  return 0;
}
