#include "hci/Keyboard.h"

#include <cstring>

#include "hci/KeyState.h"

using namespace pmd::hci;

Keys Keyboard::make_chord(const std::vector<Key>& keys)
{
  Keys ks;
  for (auto& key : keys)
  {
    ks.set(key);
  }
  return ks;
}

bool Keyboard::contains(const Keys& ks) const
{
  return ((keys & ks) ^ ks).none();
}