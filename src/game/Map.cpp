#include "game/Map.h"

#include "util/Loggers.h"

using namespace pmd::game;

Map::Map(sf::Vector2i size)
  : m_size{size}
{
  m_centers.resize(size.x * size.y);
  for (int i = 0; i < m_centers.size(); i++)
  {
    int x = (i % m_size.y);
    int y = (i / m_size.y);
    x *= Tile::Width;
    y *= Tile::Height;

    x += Tile::Width / 2;
    y += Tile::Height / 2;  

    m_centers[i].setRadius(2.0f);
    m_centers[i].setFillColor(sf::Color::Blue);
    m_centers[i].setPosition({ (float) x, (float) y });
  }
}

void Map::draw(sf::RenderTarget& target)
{
  for (int i = 0; i < m_centers.size(); i++)
  {
    target.draw(m_centers[i]);
  }
}