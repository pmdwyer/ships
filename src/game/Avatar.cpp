#include "game/Avatar.h"

using namespace pmd::game;

void Avatar::draw(sf::RenderTarget& target)
{
  sf::RenderStates states;
  states.transform.translate(screenPos);
  target.draw(shape, states);
}