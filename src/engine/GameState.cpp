#include "engine/GameState.h"

#include "util/Loggers.h"

#include <iostream>

using namespace pmd::engine;
using namespace pmd::util;

GameState::GameState()
    : m_map({28, 30})
{
}

bool GameState::init(StateManager *manager)
{
  bool inited = true;
  m_manager = manager;
  spdlog::get(Loggers::Console)->debug("called game state init");

  m_circle.setRadius(15.0f);
  m_circle.setFillColor(sf::Color::Magenta);
  m_player.pos = {0, 0};
  camera.view = m_manager->context->window.getDefaultView();
  sf::Vector2u sz = m_manager->context->window.getSize();
  camera.view.zoom(1.75f);
  camera.view.move(sz.x / -5.0f, 0.0f);

  m_manager->context->event_manager.register_for_action(Action::MoveLeft, [&] { m_circle.move(-1.0f, 0.0f); });
  m_manager->context->event_manager.register_for_action(Action::MoveRight, [&] { m_circle.move(1.0f, 0.0f); });
  m_manager->context->event_manager.register_for_action(Action::MoveUp, [&] { m_circle.move(0.0f, -1.0f); });
  m_manager->context->event_manager.register_for_action(Action::MoveDown, [&] { m_circle.move(0.0f, 1.0f); });

  return inited;
}

void GameState::start()
{
  spdlog::get(Loggers::Console)->debug("called game state start");
}

void GameState::pause()
{
}

void GameState::stop()
{
  spdlog::get(Loggers::Console)->debug("called game state stop");
}

void GameState::destroy()
{
  spdlog::get(Loggers::Console)->debug("called game state destroy");
}

void GameState::update(float dt)
{
}

void GameState::draw(sf::RenderTarget &target)
{
  m_map.draw(target);
  target.draw(m_circle);
  m_player.draw(target);
}

void GameState::pan_camera(float dx, float dy)
{
  sf::Vector2f delta_pos = sf::Vector2f{dx, dy};
  camera.view.move(delta_pos);
}