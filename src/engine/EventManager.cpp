#include "engine/EventManager.h"

using namespace pmd::engine;

bool EventManager::init()
{
  bool inited = true;
  m_key_map[ Keyboard::make_chord({Key::Left}) ] = Action::MoveLeft;
  m_key_map[ Keyboard::make_chord({Key::Right}) ] = Action::MoveRight;
  m_key_map[ Keyboard::make_chord({Key::Up}) ] = Action::MoveUp;
  m_key_map[ Keyboard::make_chord({Key::Down}) ] = Action::MoveDown;
  return inited;
}

void EventManager::record_event(const sf::Event& e)
{
  switch (e.type)
  {
    case sf::Event::KeyPressed:
      k.keys.set((int) e.key.code);
      break;
    case sf::Event::KeyReleased:
      k.keys.reset((int) e.key.code);
      break;
    case sf::Event::MouseMoved:
      m.pos = { e.mouseMove.x, e.mouseMove.y };
      check_and_update_bounds(m.pos);
      break;
    case sf::Event::MouseButtonPressed:
      m.buttons.set((int) e.mouseButton.button);
      m.pos = { e.mouseButton.x, e.mouseButton.y };
      break;
    case sf::Event::MouseButtonReleased:
      m.buttons.reset((int) e.mouseButton.button);
      m.pos = { e.mouseButton.x, e.mouseButton.y };
      break;
  }
}

void EventManager::update()
{
  for (auto& keyboard : m_key_map)
  {
    if (k.contains(keyboard.first))
    {
      check_and_fire_action(keyboard.second);
    }
  }

  if (m_mouse_loc != WindowEdges::Center)
  {
    switch (m_mouse_loc)
    {
      case WindowEdges::Left:
        check_and_fire_action(Action::PanLeft);
        break;
      case WindowEdges::Right:
        check_and_fire_action(Action::PanRight);
        break;
      case WindowEdges::Top:
        check_and_fire_action(Action::PanUp);
        break;
      case WindowEdges::Bottom:
        check_and_fire_action(Action::PanDown);
        break;
    }
  }
}

void EventManager::register_for_action(Action a, std::function<void()> cb)
{
  m_action_events[a] = cb;
}

void EventManager::check_and_update_bounds(sf::Vector2i pos)
{
  m_mouse_loc = WindowEdges::Center;
  if (pos.x < 10)
  {
    m_mouse_loc = WindowEdges::Left;
  }
  else if (pos.x > 1190)
  {
    m_mouse_loc = WindowEdges::Right;
  }
  if (pos.y < 10)
  {
    m_mouse_loc = WindowEdges::Top;
  }
  else if (pos.y > 710)
  {
    m_mouse_loc = WindowEdges::Bottom;
  }
}

void EventManager::check_and_fire_action(Action a)
{
  if (m_action_events.find(a) != m_action_events.end())
    m_action_events[a]();
}