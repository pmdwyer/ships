#include "engine/StateManager.h"

#include <cassert>

#include "engine/StartState.h"
#include "engine/GameState.h"

using namespace pmd::engine;

StateManager::~StateManager()
{
  if (context)
  {
    delete context;
  }
}

bool StateManager::init()
{
  if (!context)
  {
    context = new Context();
  }
  assert(context != nullptr);
  context->event_manager.init();
  m_state_factory[StateType::Intro] = []() -> IState* { return reinterpret_cast<IState*>(new StartState()); };
  m_state_factory[StateType::Game]  = []() -> IState* { return reinterpret_cast<IState*>(new GameState()); };
  return true;
}

void StateManager::push(StateType type)
{
  IState* state = find_or_create_state(type);
  assert(state != nullptr);
  m_states.push_back(state);
  state->start();
}

void StateManager::update(float dt)
{
  if (m_state_to_swap != StateType::Unknown)
  {
    for (auto* state : m_states)
    {
      state->stop();
    }
    m_states.clear();
    IState* nextstate = find_or_create_state(m_state_to_swap);
    m_states.push_back(m_state_cache[m_state_to_swap]);
    m_states[m_states.size() - 1]->start();
    m_state_to_swap = StateType::Unknown;
  }

  for (auto* state : m_states)
  {
    state->update(dt);
  }
}

void StateManager::switch_to(StateType type)
{
  m_state_to_swap = type;
}

void StateManager::draw(sf::RenderTarget& target)
{
  for (int i = 0; i < m_states.size(); i++)
  {
    sf::View old_view = target.getView();
    target.setView(m_states[i]->camera.view);
    m_states[i]->draw(target);
    target.setView(old_view);
  }
}

void StateManager::cleanup()
{
  for (auto state_itr : m_state_cache)
  {
    state_itr.second->stop();
    state_itr.second->destroy();
  }
}

IState* StateManager::find_or_create_state(StateType type)
{
  IState* state = nullptr;
  if (m_state_cache.find(type) == m_state_cache.end())
  {
    state = m_state_factory[type]();
    assert(state != nullptr);
    m_state_cache[type] = state;
    state->init(this);
  }
  else
  {
    state = m_state_cache[type];
  }
  return state;
}