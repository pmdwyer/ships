#include "engine/GameServer.h"

#include <chrono>

#include <spdlog/spdlog.h>

#include "util/Loggers.h"
#include "util/ThreadUtils.h"

using namespace std::chrono_literals;
using namespace pmd::engine;
using namespace pmd::util;

GameServer::~GameServer()
{
  m_localThread.join();
}

bool GameServer::init()
{
  m_isrunning = true;
  std::thread t(&GameServer::run, this);
  std::swap(t, m_localThread);
  return true;
}

void GameServer::run()
{
  while (m_isrunning)
  {
  }
}

void GameServer::stop()
{
  m_isrunning = false; // possible race condition here, but it shouldn't matter too much
}