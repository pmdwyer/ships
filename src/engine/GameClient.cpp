#include "engine/GameClient.h"

#include <spdlog/spdlog.h>
#include <imgui.h>
#include <imgui-SFML.h>

#include "engine/StateType.h"
#include "hci/Key.h"
#include "resource/FontManager.h"
#include "util/Loggers.h"
#include "util/ThreadUtils.h"

using namespace pmd::engine;
using namespace pmd::hci;
using namespace pmd::resource;
using namespace pmd::util;

bool GameClient::init()
{
  bool inited = m_state_manager.init();
  m_state_manager.context->window.create(sf::VideoMode(1200, 720), "Ships!");
  inited |= FontManager::instance().init();
  ImGui::SFML::Init(m_state_manager.context->window);
  return inited;
}

void GameClient::run()
{
  bool quit = false;
  sf::Clock clock;
  float dt = 0;
  m_state_manager.push(StateType::Game);

  sf::Text fps;
  fps.setFont(FontManager::instance().get_font("hack-reg"));
  fps.setCharacterSize(12);
  fps.setPosition({0.0, 0.0});

  while (!quit)
  {
    dt = clock.getElapsedTime().asSeconds();
    clock.restart();

    sf::Event event;

    while (m_state_manager.context->window.pollEvent(event))
    {
      ImGui::SFML::ProcessEvent(event);
      m_state_manager.context->event_manager.record_event(event);
      if (event.type == sf::Event::Closed)
        quit = true;
    }
    ImGui::SFML::Update(m_state_manager.context->window, sf::seconds(dt));
    m_state_manager.context->event_manager.update();
    
    m_state_manager.update(dt);

    m_state_manager.context->window.clear();

    fps.setString(std::to_string(dt));
    m_state_manager.context->window.draw(fps);
    m_state_manager.draw(m_state_manager.context->window);
    
    ImGui::SFML::Render(m_state_manager.context->window);

    m_state_manager.context->window.display();
  }

  m_state_manager.cleanup();
  m_state_manager.context->window.close();
}