#include "engine/StartState.h"

#include <imgui.h>

#include "util/Loggers.h"
#include "resource/FontManager.h"

using namespace pmd::engine;
using namespace pmd::resource;
using namespace pmd::util;

bool StartState::init(StateManager* manager) 
{
  m_manager = manager;
  spdlog::get(Loggers::Console)->debug("Called start state init");
  FontManager::instance().init();
  return true;
}

void StartState::start()
{
  spdlog::get(Loggers::Console)->debug("Called start state start");
}

void StartState::pause()
{
}

void StartState::stop()
{
  spdlog::get(Loggers::Console)->debug("Called start state stop");
}

void StartState::destroy()
{
  spdlog::get(Loggers::Console)->debug("Called start state destroy");
}

void StartState::update(float dt)
{
  ImGui::Begin("Menu");
  if (ImGui::Button("Start"))
  {
    spdlog::get(Loggers::Console)->debug("starting game");
    m_manager->switch_to(StateType::Game);
  }
  if (ImGui::Button("Quit"))
  {
    spdlog::get(Loggers::Console)->debug("quiting");
  }
  ImGui::End();
}

void StartState::draw(sf::RenderTarget& target)
{
}