#include "util/Loggers.h"

using namespace pmd::util;

const std::string Loggers::Console = "console";

bool Loggers::init()
{
  auto m_console = spdlog::stdout_color_mt(Loggers::Console);
  spdlog::get(Loggers::Console)->set_level(spdlog::level::debug);
  return true;
}