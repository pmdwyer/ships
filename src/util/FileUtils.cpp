#include "util/FileUtils.h"

#include <fstream>

bool pmd::util::file_exists(const std::string& path)
{
  std::ifstream stream(path);
  return stream.good();
}
