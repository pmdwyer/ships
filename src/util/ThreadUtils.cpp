#include "util/ThreadUtils.h"

#include <sstream>

std::string pmd::util::thread_id_to_str(const std::thread::id& id)
{
  std::stringstream ss;
  ss << id;
  return ss.str();
}