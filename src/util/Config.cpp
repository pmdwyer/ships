#include "util/Config.h"
#include "util/FileUtils.h"

#include <fstream>

using namespace pmd::util;

Config& Config::instance()
{
  static Config instance;
  return instance;
}

bool Config::init(const std::string& cfg_path)
{
  if (!file_exists(cfg_path))
  {
    return false;
  }

  std::ifstream cfg(cfg_path);
  cfg >> config;

  return true;
}